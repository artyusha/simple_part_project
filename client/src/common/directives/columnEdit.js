(function() {
  'use strict';

  function columnEdit() {
    return {
      restrict: 'A',
      /*jshint unused:false*/
      link: function(scope, elm, attrs) {
        var input = angular.element(elm.children()[1]);
        var inputToggle = function(){
          elm.children().toggleClass("hidden");
          if (!input.hasClass("hidden")) {
            input[0].focus();  
          }
        }
        var inputShortCuts = function($event){
          if ($event.keyCode == 9) {
            if ($event.shiftKey) {
              var prevCol = elm.prev();
              if (prevCol.length>0) {
                inputToggleBlur();
                setTimeout(function(){
                  prevCol.trigger("click");
                }, 0);
              }
            } else {
              var nextCol = elm.next();
              if (nextCol.length>0) {
                inputToggleBlur();
                setTimeout(function(){
                  nextCol.trigger("click");
                }, 0);
              }
            }
            $event.preventDefault();
          } else if ($event.keyCode == 13) {
            if ($event.altKey) {
              var prevRow = elm.parent().prev();
              if (prevRow.length>0) {
                inputToggleBlur();
                setTimeout(function(){
                  prevRow.children().eq(elm.index()).trigger("click");
                }, 0);
              }
            } else if ($event.ctrlKey) {
              var nextRow = elm.parent().next();
              if (nextRow.length>0) {
                inputToggleBlur();
                setTimeout(function(){
                  nextRow.children().eq(elm.index()).trigger("click");
                }, 0);
              }
            }
          } else if ($event.keyCode == 46 && $event.ctrlKey) {
            inputToggleBlur();
            console.log(scope.create.tableData);
            console.log(attrs["columnEdit"]);
            scope.create.tableData.splice(attrs["columnEdit"],1);
            scope.$apply();
          } else {
            console.log($event.keyCode);
          }
        }
        var inputToggleClick = function(){
          inputToggle();
          elm.off("click");
          input.on("blur", inputToggleBlur)
          input.on("keydown", inputShortCuts)
        }
        var inputToggleBlur = function(){
          // if(scope.checkValid(attrs["columnEdit"])){
            
          // }
          // return
          inputToggle();
          input.off("blur");
          input.off("keydown");
          elm.on("click", inputToggleClick)
          
        }

        elm.on("click", inputToggleClick)
      }
    };
  }

  angular.module('common.directives.columnEdit', [])
    .directive('columnEdit', columnEdit);
})();
