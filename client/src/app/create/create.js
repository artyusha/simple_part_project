(function() {
  'use strict';

  /**
   * @name  config
   * @description config block
   */
  function config($stateProvider) {
    $stateProvider
      .state('root.create', {
        url: '/create',
        views: {
          '@': {
            templateUrl: 'src/app/create/create.tpl.html',
            controller: 'CreateCtrl as create',
            resolve: {
              excelService: function (ExcelService) {
                return ExcelService;
              },
              backendService: function(BackendService) {
                return BackendService;
              }
            }
          }
        }
      });
  }

  /**
   * @name  CreateCtrl
   * @description Controller
   */
  function CreateCtrl(excelService, backendService, $state) {
    var create = this;
    create.tableData = [];
    create.checkMyCode = function($event){
      var data = excelService.createData(create.excelData);
      if (!data.validity) {
        alert("There is an error in your excel file");
        return
      } else {
        console.log(data);
      }
      create.tableData = create.tableData.concat(data.table);
      create.tableData = excelService.mergeDuplicateRows({
        validity: true,
        table: create.tableData
      }).table;
      create.excelData = "";
    }
    create.sendDataToBackend = function ($event) {
      backendService.create({"parts":create.tableData}).then(function(){
        $state.go("root.home");
      })
    }
  }

  angular.module('create', [])
    .config(config)
    .controller('CreateCtrl', CreateCtrl);
})();
