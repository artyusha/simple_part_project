(function() {
  'use strict';

  function backendService($http) {
    return {
      create: function(data) {

        return $http.post(server + "api/automobile/parts", data, {

        	headers: {
        		"Content-Type":"multipart/form-data",
        		"Accept": "application/json",
        	}
        });
      },
      fetch: function() {
        return $http.get(server + "api/automobile/parts");
      }
    };
  }

  angular.module('common.services.backend', [])
    .factory('BackendService', backendService);
})();
