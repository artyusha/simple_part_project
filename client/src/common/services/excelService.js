(function() {
  'use strict';

  function excelService() {
    var Row = function (data) {
      this.description = data.description;
      this.number = data.number;
      this.brand_name = data.brand_name;
      this.quantity = data.quantity;
      this.price = data.price;
      this.condition = data.condition;
      this.location = data.location;
    };
    var parseData = function(data) {
      return data.split("\n").map(function (row) {
        return row.split("\t");
      });
    };
    var turnDataToObjects = function(data) {
      var dataValidity = true;
      data = data.map(function (rowArr) {
          var row = {
            "description": rowArr[0],
            "number": rowArr[1],
            "brand_name": rowArr[2],
            "quantity": parseInt(rowArr[3]),
            "price": rowArr[4],
            "condition": rowArr[5],
            "location": rowArr[6]
          };
          var rowValidity = isDataValid(row);
          if(rowValidity !== true) {
            dataValidity = false;
          }
          return row; 
        })
      return {
        validity: dataValidity,
        table: data
      }
    }
    var isDataValid = function(data){
      if (Number.isNaN(Number(data.number))) {
        return "Part Number is not valid";
      } else if(Number.isNaN(Number(data.quantity))) {
        return "Quantity is not valid";
      } else if(Number.isNaN(Number(data.price))) {
        return "Price is not valid";
      }
      return true;
    }
    return {
      createData: function(data) {
        data = parseData(data);
        data = turnDataToObjects(data);
        if (data.validity === true) {
          data = this.mergeDuplicateRows(data);
          return data;
        }
        return data;

      },
      mergeDuplicateRows: function(data) {
        var dataArray = data.table;
        var result = [];
      
        dataArray.forEach(function (a) {
            if (!this[a.number]) {
                this[a.number] = new Row({
                  "description": a.description,
                  "number": parseInt(a.number),
                  "brand_name": a.brand_name,
                  "quantity": 0,
                  "price": parseInt(a.price),
                  "condition": a.condition,
                  "location": a.location
                });
                result.push(this[a.number]);
            }
            this[a.number].quantity = parseInt(this[a.number].quantity) + parseInt(a.quantity);
        }, Object.create(null));

        return {validity: data.validity, table:result};
      }


    };
  }

  angular.module('common.services.excel', [])
    .factory('ExcelService', excelService);
})();
