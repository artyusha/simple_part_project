(function() {
  'use strict';

  /**
   * @name  config
   * @description config block
   */
  function config($stateProvider) {
    $stateProvider
      .state('root.home', {
        url: '/',
        views: {
          '@': {
            templateUrl: 'src/app/home/home.tpl.html',
            controller: 'HomeCtrl as home',
            resolve: {
              excelService: function (ExcelService) {
                return ExcelService;
              },
              backendService: function(BackendService) {
                return BackendService;
              }
            }
          }
        }
      });
  }

  /**
   * @name  HomeCtrl
   * @description Controller
   */
  function HomeCtrl(excelService, backendService) {
    var home = this;
    home.tableData = [];
    backendService.fetch().then(function(data){
      home.table = data;
    })
  }

  angular.module('home', [])
    .config(config)
    .controller('HomeCtrl', HomeCtrl);
})();

